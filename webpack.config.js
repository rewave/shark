var path = require("path");

module.exports = {
  entry: path.resolve("./src/index.js"),
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "index.js",
    libraryTarget: "commonjs2"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve("./src"),
        exclude: /(node_modules|bower_components|build)/,
        use: {
          // loader: "babel-loader",
          loader: path.resolve(__dirname, "./node_modules/babel-loader"),
          options: {
            presets: [
              path.resolve(__dirname, "./node_modules/@babel/preset-env"),
              path.resolve(__dirname, "./node_modules/@babel/preset-react")
            ]
          }
        }
      },
      {
        test: /\.css$/,
        use: [
          path.resolve(__dirname, "./node_modules/style-loader"),
          path.resolve(__dirname, "./node_modules/css-loader")
        ]
      }
    ]
  },
  externals: {
    react: "commonjs react"
  }
};
