import React from "react";
import ReactDOM from "react-dom";

import defaultEnv from "./default-env";

import Editor, { Editable, createEmptyState } from "@react-page/core";
import "@react-page/core/lib/index.css";

import { HTMLRenderer } from "@react-page/renderer";

import { Trash, DisplayModeToggle, Toolbar } from "@react-page/ui";
import "@react-page/ui/lib/index.css";

import slate from "@react-page/plugins-slate";
import "@react-page/plugins-slate/lib/index.css";

import spacer from "@react-page/plugins-spacer";
import "@react-page/plugins-spacer/lib/index.css";

import image from "@react-page/plugins-image";
import "@react-page/plugins-image/lib/index.css";

import form from "./plugins/form";
import iframe from "./plugins/iframe";
import button from "./plugins/button";

const PROJECT_ID = process.env.REACT_APP_PROJECT_ID || defaultEnv.PROJECT_ID;
const BUILDER_MODE =
  process.env.REACT_APP_BUILDER_MODE || defaultEnv.BUILDER_MODE;
const IS_EMBEDDED = process.env.REACT_APP_IS_EMBEDDED || defaultEnv.IS_EMBEDDED;

const projectPlugins = {
  CARTRAVEN: { content: [slate(), spacer, image, form] },
  ADVERTORIALLY: { content: [slate(), spacer, image, iframe, button] }
};

const content = createEmptyState();

const editor = new Editor({
  plugins: projectPlugins[PROJECT_ID],
  defaultPlugin: form,
  editables: [content]
});

class Shark extends React.Component {
  componentDidMount() {
    console.log(1);
  }

  render() {
    return (
      <div>
        {BUILDER_MODE === "TRUE" ? (
          <React.Fragment>
            <Editable
              editor={editor}
              id={content.id}
              onChange={editable => {
                console.log(editable);
              }}
            />

            <Trash editor={editor} />
            <DisplayModeToggle editor={editor} />
            <Toolbar editor={editor} />
          </React.Fragment>
        ) : (
          <HTMLRenderer state={{}} plugins={projectPlugins[PROJECT_ID]} />
        )}
      </div>
    );
  }
}

if (IS_EMBEDDED === "TRUE") {
  ReactDOM.render(<Shark />, document.getElementById("shark-wrapper"));
}

export default Shark;
