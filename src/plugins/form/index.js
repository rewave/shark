import React from "react";
import TextField from "@material-ui/core/TextField";
import { BottomToolbar } from "@react-page/ui";
import {
  darkTheme,
  default as ThemeProvider
} from "@react-page/ui/lib/ThemeProvider";

import defaultState from "./default-state";

const inputWrapperStyle = {
  marginBottom: 20
};

const FormComponent = props => (
  <React.Fragment>
    <Renderer state={props.state} />

    {!props.readOnly && props.focused && (
      <ThemeProvider theme={darkTheme}>
        <BottomToolbar open={props.focused} theme={darkTheme}>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: First name"
              label="First name input placeholder"
              name="first_name_placeholder"
              style={{ flex: 1, width: 300 }}
              value={
                props.state.firstNamePlaceholder ||
                defaultState.firstNamePlaceholder
              }
              onChange={e => {
                props.onChange({
                  firstNamePlaceholder: e.target.value
                });
              }}
            />
            <TextField
              placeholder="E.g.: Last name"
              label="Last name input placeholder"
              name="last_name_placeholder"
              style={{ flex: 1, width: 300 }}
              value={
                props.state.lastNamePlaceholder ||
                defaultState.lastNamePlaceholder
              }
              onChange={e => {
                props.onChange({
                  lastNamePlaceholder: e.target.value
                });
              }}
            />
          </div>
        </BottomToolbar>
      </ThemeProvider>
    )}
  </React.Fragment>
);

class Renderer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: ""
    };
  }

  render() {
    return (
      <form
        onSubmit={e => {
          e.preventDefault();
          alert("Submitted!");
        }}
        style={{ marginTop: 20 }}
      >
        <div style={inputWrapperStyle}>
          <input
            type="text"
            className="input"
            placeholder={this.props.state.firstNamePlaceholder || "First name"}
            value={this.state.firstName}
            onChange={e =>
              this.setState({
                firstName: e.target.value
              })
            }
          />
        </div>

        <div style={inputWrapperStyle}>
          <input
            type="text"
            className="input"
            placeholder={this.props.state.lastNamePlaceholder || "Last name"}
            value={this.state.lastName}
            onChange={e =>
              this.setState({
                lastName: e.target.value
              })
            }
          />
        </div>

        <div>
          <button type="submit" value="Submit">
            Submit
          </button>
        </div>
      </form>
    );
  }
}

export default {
  Component: props => <FormComponent {...props} />,
  // IconComponent: <CropSquare />,
  name: "builder/layout/form",
  version: "0.0.1",
  text: "Form"
};
