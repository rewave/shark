import React from "react";
import TextField from "@material-ui/core/TextField";
import { BottomToolbar } from "@react-page/ui";
import {
  darkTheme,
  default as ThemeProvider
} from "@react-page/ui/lib/ThemeProvider";

import defaultState from "./default-state";

const IFrameComponent = props => (
  <React.Fragment>
    <Renderer
      state={props.state}
      readOnly={props.readOnly}
      focused={props.focused}
    />

    {!props.readOnly && props.focused && (
      <ThemeProvider theme={darkTheme}>
        <BottomToolbar open={props.focused} theme={darkTheme}>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: https://rewave.io"
              label="IFrame URL"
              name="iframe_url"
              style={{ flex: 1, width: 300 }}
              value={props.state.iframeUrl || defaultState.iframeUrl}
              onChange={e => {
                props.onChange({
                  iframeUrl: e.target.value
                });
              }}
            />
          </div>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: 600"
              label="Width"
              name="width"
              style={{ flex: 1, width: 300 }}
              value={props.state.width || defaultState.width}
              onChange={e => {
                props.onChange({
                  width: e.target.value
                });
              }}
            />
          </div>
        </BottomToolbar>
      </ThemeProvider>
    )}
  </React.Fragment>
);

class Renderer extends React.Component {
  render() {
    return (
      <div
        dangerouslySetInnerHTML={{
          __html: `<iframe src="${this.props.state.iframeUrl ||
            defaultState.iframeUrl}" width="600" height="350"></iframe>`
        }}
      />
    );
  }
}

export default {
  Component: props => <IFrameComponent {...props} />,
  // IconComponent: <CropSquare />,
  name: "builder/layout/iframe",
  version: "0.0.1",
  text: "IFrame"
};
