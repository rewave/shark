import React from "react";
import TextField from "@material-ui/core/TextField";
import { BottomToolbar } from "@react-page/ui";
import {
  darkTheme,
  default as ThemeProvider
} from "@react-page/ui/lib/ThemeProvider";

import defaultState from "./default-state";

const ButtonComponent = props => (
  <React.Fragment>
    <Renderer
      state={props.state}
      readOnly={props.readOnly}
      focused={props.focused}
    />

    {!props.readOnly && props.focused && (
      <ThemeProvider theme={darkTheme}>
        <BottomToolbar open={props.focused} theme={darkTheme}>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: button button-large"
              label="Classes"
              name="classes"
              style={{ flex: 1, width: 300 }}
              value={props.state.classes || defaultState.classes}
              onChange={e => {
                props.onChange({
                  classes: e.target.value
                });
              }}
            />
          </div>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: Submit"
              label="Text"
              name="text"
              style={{ flex: 1, width: 300 }}
              value={props.state.text || defaultState.text}
              onChange={e => {
                props.onChange({
                  text: e.target.value
                });
              }}
            />
          </div>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: https://rewave.io"
              label="URL to open"
              name="url"
              style={{ flex: 1, width: 300 }}
              value={props.state.url || defaultState.url}
              onChange={e => {
                props.onChange({
                  url: e.target.value
                });
              }}
            />
          </div>
        </BottomToolbar>
      </ThemeProvider>
    )}
  </React.Fragment>
);

class Renderer extends React.Component {
  render() {
    return (
      <a
        className={this.props.state.classes || defaultState.classes}
        href={this.props.state.url || defaultState.url}
      >
        {this.props.state.text || defaultState.text}
      </a>
    );
  }
}

export default {
  Component: props => <ButtonComponent {...props} />,
  // IconComponent: <CropSquare />,
  name: "builder/layout/button",
  version: "0.0.1",
  text: "Button"
};
