import React from "react";
import TextField from "@material-ui/core/TextField";
import { BottomToolbar } from "ory-editor-ui";
import {
  darkTheme,
  default as ThemeProvider
} from "ory-editor-ui/lib/ThemeProvider";

import defaultState from "./default-state";

const TestimonialComponent = props => (
  <React.Fragment>
    <Renderer
      state={props.state}
      readOnly={props.readOnly}
      focused={props.focused}
    />

    {!props.readOnly && props.focused && (
      <ThemeProvider theme={darkTheme}>
        <BottomToolbar open={props.focused} theme={darkTheme}>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: https://rewave.io/img/john-smith.jpg"
              label="Thumbnail URL"
              name="thumbnail"
              style={{ flex: 1, width: 300 }}
              value={props.state.thumbnailUrl || defaultState.thumbnailUrl}
              onChange={e => {
                props.onChange({
                  thumbnailUrl: e.target.value
                });
              }}
            />
          </div>
          <div style={{ display: "flex" }}>
            <TextField
              placeholder="E.g.: Rewave is an amazing company, I could not find a better one."
              label="Text"
              name="text"
              style={{ flex: 1, width: 300 }}
              value={props.state.text || defaultState.text}
              onChange={e => {
                props.onChange({
                  text: e.target.value
                });
              }}
            />
          </div>
        </BottomToolbar>
      </ThemeProvider>
    )}
  </React.Fragment>
);

class Renderer extends React.Component {
  render() {
    return (
      <div>
        <div>
          <img
            src={this.props.state.thumbnailUrl || defaultState.thumbnailUrl}
            alt=""
          />
        </div>

        <div>{this.props.state.text || defaultState.text}</div>
      </div>
    );
  }
}

export default {
  Component: props => <TestimonialComponent {...props} />,
  // IconComponent: <CropSquare />,
  name: "builder/layout/testimonial",
  version: "0.0.1",
  text: "Testimonial quote"
};
